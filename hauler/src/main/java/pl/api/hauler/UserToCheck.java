package pl.api.hauler;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import pl.api.hauler.models.Address;
import pl.api.hauler.models.User;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class UserToCheck implements UserDetails {

    private int id;
    private String userName;
    private String name;
    private String surname;
    private Address address;
    private String password;
    private List<GrantedAuthority> authorities;

    public UserToCheck(User user) {
        this.id = user.getId();
        this.userName = user.getLogin();
        this.name = user.getName();
        this.surname = user.getSurname();
        this.address = user.getAddress();
        this.password = user.getPassword();
        this.authorities = Arrays.stream(user.getPrivileges().split(","))
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return userName;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
