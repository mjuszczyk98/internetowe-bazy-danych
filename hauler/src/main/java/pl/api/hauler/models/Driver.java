package pl.api.hauler.models;

import javax.persistence.*;

@Entity
@Table(name = "DRIVERS")
public class Driver {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "DRIVER_ID")
    private int id;
    @Column(name = "NAME")
    private String name;
    @Column(name = "SURNAME")
    private String surname;
    @Column(name = "GENDER")
    private String gender;
    @OneToOne
    @JoinColumn(name = "ADDRESS_ID")
    private Address address;
    @Column(name = "DOCUMENT_NR")
    private String documentId;
    @Column(name = "DRIVING_LICENSE_TYPE")
    private String drivingLicense;
    @Column(name = "DRIVER_LICENSE_NR")
    private String drivingLicenseId;
    @Column(name = "PHONE_NR")
    private String phone;

    public Driver() {
    }

    public Driver(int id, String name, String surname, String gender, Address address, String documentId, String drivingLicense, String drivingLicenseId, String phone) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.gender = gender;
        this.address = address;
        this.documentId = documentId;
        this.drivingLicense = drivingLicense;
        this.drivingLicenseId = drivingLicenseId;
        this.phone = phone;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public String getDrivingLicense() {
        return drivingLicense;
    }

    public void setDrivingLicense(String drivingLicense) {
        this.drivingLicense = drivingLicense;
    }

    public String getDrivingLicenseId() {
        return drivingLicenseId;
    }

    public void setDrivingLicenseId(String drivingLicenseId) {
        this.drivingLicenseId = drivingLicenseId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
