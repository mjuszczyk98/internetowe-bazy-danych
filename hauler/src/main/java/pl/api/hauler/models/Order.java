package pl.api.hauler.models;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "ORDERS")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ORDER_ID")
    private int id;
    @OneToOne
    @JoinColumn(name = "source_address")
    private Address sourceAddress;
    @OneToOne
    @JoinColumn(name = "destination_address")
    private Address destinationAddress;
    @Column(name = "date_of_shipment")
    private Date shipmentDate;
    @Column(name = "date_of_delivery")
    private Date deliveryDate;
    @OneToOne
    @JoinColumn(name = "contractor")
    private Contractor contractor;
    @JoinColumn(name = "good")
    @OneToOne
    private Good good;
    @Column(name = "description")
    private String description;
    @Column(name = "weight")
    private double weight;
    @Column(name = "pallet_places_count")
    private int palletCounter;
    @Column(name = "price")
    private double price;
    @OneToOne
    @JoinColumn(name = "driver")
    Driver driver;
    @OneToOne
    @JoinColumn(name = "user")
    User user;

    public Order() {
    }

    public Order(int id, Address sourceAddress, Address destinationAddress, Date shipmentDate, Date deliveryDate, Contractor contractor, Good good, String description, double weight, int palletCounter, double price, Driver driver, User user) {
        this.id = id;
        this.sourceAddress = sourceAddress;
        this.destinationAddress = destinationAddress;
        this.shipmentDate = shipmentDate;
        this.deliveryDate = deliveryDate;
        this.contractor = contractor;
        this.good = good;
        this.description = description;
        this.weight = weight;
        this.palletCounter = palletCounter;
        this.price = price;
        this.driver = driver;
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Address getSourceAddress() {
        return sourceAddress;
    }

    public void setSourceAddress(Address sourceAddress) {
        this.sourceAddress = sourceAddress;
    }

    public Address getDestinationAddress() {
        return destinationAddress;
    }

    public void setDestinationAddress(Address destinationAddress) {
        this.destinationAddress = destinationAddress;
    }

    public Date getShipmentDate() {
        return shipmentDate;
    }

    public void setShipmentDate(Date shipmentDate) {
        this.shipmentDate = shipmentDate;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Contractor getContractor() {
        return contractor;
    }

    public void setContractor(Contractor contractor) {
        this.contractor = contractor;
    }

    public Good getGood() {
        return good;
    }

    public void setGood(Good good) {
        this.good = good;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public int getPalletCounter() {
        return palletCounter;
    }

    public void setPalletCounter(int palletCounter) {
        this.palletCounter = palletCounter;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
