package pl.api.hauler.models;

import javax.persistence.*;

@Entity
@Table(name = "GOODS")
public class Good {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "GOOD_ID")
    private int id;
    @Column(name = "NAME")
    private String name;
    @OneToOne
    @JoinColumn(name = "ADR")
    ADR adr;

    public Good() {
    }

    public Good(int id, String name, ADR adr) {
        this.id = id;
        this.name = name;
        this.adr = adr;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ADR getADR() {
        return adr;
    }

    public void setADR(ADR adr) {
        this.adr = adr;
    }
}
