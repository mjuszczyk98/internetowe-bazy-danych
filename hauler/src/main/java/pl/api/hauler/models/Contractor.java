package pl.api.hauler.models;

import javax.persistence.*;

@Entity
@Table(name = "CONTRACTORS")
public class Contractor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CONTRACTOR_ID")
    private int id;
    @Column(name = "NAME")
    private String name;
    @OneToOne
    @JoinColumn(name = "ADDRESS_ID")
    private Address address;
    @Column(name = "NIP")
    private String NIP;
    @Column(name = "PHONE_NR")
    private String phone;
    @Column(name = "EMAIL")
    private String email;

    public Contractor() {
    }

    public Contractor(int id, String name, Address address, String NIP, String phone, String email) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.NIP = NIP;
        this.phone = phone;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getNIP() {
        return NIP;
    }

    public void setNIP(String NIP) {
        this.NIP = NIP;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
