package pl.api.hauler.models;

import javax.persistence.*;

@Entity
@Table(name = "ADRS")
public class ADR {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ADR_ID")
    private int id;
    @Column(name = "ADR_NR")
    private double number;
    @Column(name = "DESCRIPTION")
    private String description;

    public ADR() {
    }

    public ADR(int id, double number, String description) {
        this.id = id;
        this.number = number;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getNumber() {
        return number;
    }

    public void setNumber(double number) {
        this.number = number;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
