package pl.api.hauler.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.api.hauler.models.Good;

public interface GoodRepository extends JpaRepository<Good, Integer> {
}
