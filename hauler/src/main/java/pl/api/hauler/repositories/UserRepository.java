package pl.api.hauler.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.api.hauler.models.User;

@Repository
public interface UserRepository extends CrudRepository<User,Long> {
    User getUserByUsername(String username);
}
