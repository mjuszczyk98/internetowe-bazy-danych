package pl.api.hauler.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.api.hauler.models.Order;

public interface OrderRepository extends JpaRepository<Order, Integer> {
}
