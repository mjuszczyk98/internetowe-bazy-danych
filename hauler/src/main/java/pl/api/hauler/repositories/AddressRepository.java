package pl.api.hauler.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.api.hauler.models.Address;

public interface AddressRepository extends JpaRepository<Address, Integer> {
}
