package pl.api.hauler.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.api.hauler.models.ADR;

public interface ADRrepository extends JpaRepository<ADR, Integer> {
}
