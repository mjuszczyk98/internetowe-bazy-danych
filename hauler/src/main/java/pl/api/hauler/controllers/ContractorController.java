package pl.api.hauler.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.api.hauler.models.Contractor;
import pl.api.hauler.models.Driver;
import pl.api.hauler.repositories.AddressRepository;
import pl.api.hauler.repositories.ContractorRepository;

import java.util.Optional;

@RestController
@RequestMapping("/contractor")
public class ContractorController {
    
    private final ContractorRepository contractorRepository;
    private final AddressRepository addressRepository;

    public ContractorController(ContractorRepository contractorRepository, AddressRepository addressRepository) {
        this.contractorRepository = contractorRepository;
        this.addressRepository = addressRepository;
    }

    @GetMapping()
    public ResponseEntity getAllContractors(){
        return new ResponseEntity(this.contractorRepository.findAll(), HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity insertContractor(@RequestBody Contractor contractor){
        contractor.setAddress(this.addressRepository.save(contractor.getAddress()));
        return new ResponseEntity(this.contractorRepository.save(contractor), HttpStatus.OK);
    }

    @PutMapping()
    public ResponseEntity updateContractor(@RequestBody Contractor contractor){
        if(this.contractorRepository.existsById(contractor.getId())){
            contractor.setAddress(this.addressRepository.save(contractor.getAddress()));
            return new ResponseEntity(this.contractorRepository.save(contractor), HttpStatus.OK);
        }
        else{
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity getContractor(@PathVariable int id){
        return new ResponseEntity(this.contractorRepository.findById(id), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteContractor(@PathVariable int id){
        Optional<Contractor> contractor = this.contractorRepository.findById(id);
        this.contractorRepository.deleteById(id);
        this.addressRepository.deleteById(contractor.get().getAddress().getId());
        return new ResponseEntity(contractor.get(), HttpStatus.OK);
    }
}
