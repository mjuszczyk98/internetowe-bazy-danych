package pl.api.hauler.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import pl.api.hauler.models.User;
import pl.api.hauler.repositories.AddressRepository;
import pl.api.hauler.repositories.UserRepository;

import java.util.Optional;

@RestController
@RequestMapping("/user")
public class UserController {

    private final UserRepository userRepository;
    private final AddressRepository addressRepository;
    private final PasswordEncoder passwordEncoder;

    public UserController(UserRepository userRepository, AddressRepository addressRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.addressRepository = addressRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping()
    public ResponseEntity getAllUsers(){
        return new ResponseEntity(this.userRepository.findAll(), HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity insertUser(@RequestBody User user){
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setAddress(this.addressRepository.save(user.getAddress()));
        return new ResponseEntity(this.userRepository.save(user), HttpStatus.OK);
    }

    @PutMapping()
    public ResponseEntity updateUser(@RequestBody User user){
        if(this.userRepository.existsById(user.getId())){
            user.setAddress(this.addressRepository.save(user.getAddress()));
            return new ResponseEntity(this.userRepository.save(user), HttpStatus.OK);
        }
        else{
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity getUser(@PathVariable Long id){
        return new ResponseEntity(this.userRepository.findById(id), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteUser(@PathVariable Long id){
        Optional<User> user = this.userRepository.findById(id);
        this.userRepository.deleteById(id);
        this.addressRepository.deleteById(user.get().getAddress().getId());
        return new ResponseEntity(user.get(), HttpStatus.OK);
    }
}
