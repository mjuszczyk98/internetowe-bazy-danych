package pl.api.hauler.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.api.hauler.models.Order;
import pl.api.hauler.repositories.AddressRepository;
import pl.api.hauler.repositories.OrderRepository;

import java.util.Optional;

@RestController
@RequestMapping("/order")
public class OrderController {
    
    private final OrderRepository orderRepository;
    private final AddressRepository addressRepository;

    public OrderController(OrderRepository orderRepository, AddressRepository addressRepository) {
        this.orderRepository = orderRepository;
        this.addressRepository = addressRepository;
    }

    @GetMapping()
    public ResponseEntity getAllOrders(){
        return new ResponseEntity(this.orderRepository.findAll(), HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity insertOrder(@RequestBody Order order){
        order.setDestinationAddress(this.addressRepository.save(order.getDestinationAddress()));
        order.setSourceAddress(this.addressRepository.save(order.getSourceAddress()));
        return new ResponseEntity(this.orderRepository.save(order), HttpStatus.OK);
    }

    @PutMapping()
    public ResponseEntity updateOrder(@RequestBody Order order){
        if(this.orderRepository.existsById(order.getId())){
            order.setDestinationAddress(this.addressRepository.save(order.getDestinationAddress()));
            order.setSourceAddress(this.addressRepository.save(order.getSourceAddress()));
            return new ResponseEntity(this.orderRepository.save(order), HttpStatus.OK);
        }
        else{
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity getOrder(@PathVariable int id){
        return new ResponseEntity(this.orderRepository.findById(id), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteOrder(@PathVariable int id){
        Optional<Order> order = this.orderRepository.findById(id);
        this.orderRepository.deleteById(id);
        this.addressRepository.deleteById(order.get().getDestinationAddress().getId());
        this.addressRepository.deleteById(order.get().getSourceAddress().getId());
        return new ResponseEntity(order.get(), HttpStatus.OK);
    }
}
