package pl.api.hauler.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.api.hauler.models.ADR;
import pl.api.hauler.repositories.ADRrepository;

import java.util.Optional;

@RestController
@RequestMapping("/adr")
public class ADRcontroller {
    
    private final ADRrepository adrRepository;

    public ADRcontroller(ADRrepository adrRepository) {
        this.adrRepository = adrRepository;
    }

    @GetMapping()
    public ResponseEntity getAllADRs(){
        return new ResponseEntity(this.adrRepository.findAll(), HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity insertADR(@RequestBody ADR adr){
        return new ResponseEntity(this.adrRepository.save(adr), HttpStatus.OK);
    }

    @PutMapping()
    public ResponseEntity updateADR(@RequestBody ADR adr){
        if(this.adrRepository.existsById(adr.getId())){
            return new ResponseEntity(this.adrRepository.save(adr), HttpStatus.OK);
        }
        else{
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity getADR(@PathVariable int id){
        return new ResponseEntity(this.adrRepository.findById(id), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteADR(@PathVariable int id){
        Optional<ADR> adr = this.adrRepository.findById(id);
        this.adrRepository.deleteById(id);
        return new ResponseEntity(adr.get(), HttpStatus.OK);
    }
}
