package pl.api.hauler.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.api.hauler.models.Address;
import pl.api.hauler.repositories.AddressRepository;

import java.util.Optional;

@RestController
@RequestMapping("/address")
public class AddressController {

    private final AddressRepository addressRepository;

    public AddressController(AddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }

    @GetMapping()
    public ResponseEntity getAllAddresses(){
        return new ResponseEntity(this.addressRepository.findAll(), HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity insertAddress(@RequestBody Address address){
        return new ResponseEntity(this.addressRepository.save(address), HttpStatus.OK);
    }

    @PutMapping()
    public ResponseEntity updateAddress(@RequestBody Address address){
        if(this.addressRepository.existsById(address.getId())){
            return new ResponseEntity(this.addressRepository.save(address), HttpStatus.OK);
        }
        else{
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity getAddress(@PathVariable int id){
        return new ResponseEntity(this.addressRepository.findById(id), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteAddress(@PathVariable int id){
        Optional<Address> address = this.addressRepository.findById(id);
        this.addressRepository.deleteById(id);
        return new ResponseEntity(address.get(), HttpStatus.OK);
    }
}
