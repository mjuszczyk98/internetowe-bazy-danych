package pl.api.hauler.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.api.hauler.models.Good;
import pl.api.hauler.repositories.GoodRepository;

import java.util.Optional;

@RestController
@RequestMapping("/good")
public class GoodController {
    
    private final GoodRepository goodRepository;

    public GoodController(GoodRepository goodRepository) {
        this.goodRepository = goodRepository;
    }

    @GetMapping()
    public ResponseEntity getAllGoods(){
        return new ResponseEntity(this.goodRepository.findAll(), HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity insertGood(@RequestBody Good good){
        return new ResponseEntity(this.goodRepository.save(good), HttpStatus.OK);
    }

    @PutMapping()
    public ResponseEntity updateGood(@RequestBody Good good){
        if(this.goodRepository.existsById(good.getId())){
            return new ResponseEntity(this.goodRepository.save(good), HttpStatus.OK);
        }
        else{
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity getGood(@PathVariable int id){
        return new ResponseEntity(this.goodRepository.findById(id), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteGood(@PathVariable int id){
        Optional<Good> good = this.goodRepository.findById(id);
        this.goodRepository.deleteById(id);
        return new ResponseEntity(good.get(), HttpStatus.OK);
    }
}
