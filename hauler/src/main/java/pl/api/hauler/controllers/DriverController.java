package pl.api.hauler.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.api.hauler.models.Address;
import pl.api.hauler.models.Driver;
import pl.api.hauler.repositories.AddressRepository;
import pl.api.hauler.repositories.DriverRepository;

import java.util.Optional;

@RestController
@RequestMapping("/driver")
public class DriverController {
    
    private final DriverRepository driverRepository;
    private final AddressRepository addressRepository;

    public DriverController(DriverRepository driverRepository, AddressRepository addressRepository) {
        this.driverRepository = driverRepository;
        this.addressRepository = addressRepository;
    }

    @GetMapping()
    public ResponseEntity getAllDrivers(){
        return new ResponseEntity(this.driverRepository.findAll(), HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity insertDriver(@RequestBody Driver driver){
        driver.setAddress(this.addressRepository.save(driver.getAddress()));
        return new ResponseEntity(this.driverRepository.save(driver), HttpStatus.OK);
    }

    @PutMapping()
    public ResponseEntity updateDriver(@RequestBody Driver driver){
        if(this.driverRepository.existsById(driver.getId())){
            driver.setAddress(this.addressRepository.save(driver.getAddress()));
            return new ResponseEntity(this.driverRepository.save(driver), HttpStatus.OK);
        }
        else{
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity getDriver(@PathVariable int id){
        return new ResponseEntity(this.driverRepository.findById(id), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteDriver(@PathVariable int id){
        Optional<Driver> driver = this.driverRepository.findById(id);
        this.driverRepository.deleteById(id);
        this.addressRepository.deleteById(driver.get().getAddress().getId());
        return new ResponseEntity(driver.get(), HttpStatus.OK);
    }
}
