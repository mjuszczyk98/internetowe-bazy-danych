# Projekt internetowe bazy danych

## Przygotowanie środowiska

### Instalacja node.js

Pobierz i zainstaluj https://nodejs.org/dist/v14.15.0/node-v14.15.0-x64.msi

### Instalacja Angulara

Po zainstalowaniu node.js, otwórz konsolę i wykonaj komendę `npm install -g @angular/cli`

### Instalacja pakietów

Otwórz konsole i udaj się do głównego folderu projektu. Następnie uruchom komendę `npm install`
Nie używaj `npm update` jeżeli nie musisz

## Uruchamianie aplikacji

Za pomoca konsoli uruchom komende `ng serve` lub `npm start` znajdując się w głównym folderze projektu
Uruchamiana aplikacja będzie dostępna pod adresem `localhost:4200`
