import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Contractor, Driver, Good, Order, User } from 'src/app/models';
import { ContractorService } from 'src/app/services/constactor.service';
import { DriverService } from 'src/app/services/driver.service';
import { GoodService } from 'src/app/services/good.service';
import { LoginService } from 'src/app/services/login.service';
import { OrderService } from 'src/app/services/order.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html'
})
export class OrderComponent implements OnInit {
  drivers: Observable<Driver[]>;
  orders: Observable<Order[]>;
  goods: Observable<Good[]>;
  contractors: Observable<Contractor[]>;
  selectedOrder: Order;
  user: User;
  deleteEnabled = false;
  editEnabled: Subject<boolean>;

  constructor(
    private driverService: DriverService,
    private contractorService: ContractorService,
    private orderService: OrderService,
    private goodService: GoodService,
    private loginService: LoginService,
  ) {
    this.selectedOrder = {} as Order;
  }

  ngOnInit(): void {
    this.drivers = this.driverService.get();
    this.orders = this.orderService.get();
    this.goods = this.goodService.get();
    this.contractors = this.contractorService.get();
    this.user = this.loginService.getUser();
    this.editEnabled = this.loginService.getSubject();
  }

  selectOrder(order: Order): void {
    this.selectedOrder = order;
    this.deleteEnabled = true;
  }

  addNew(): void {
    this.selectedOrder = {} as Order;
    this.deleteEnabled = false;
  }

  delete(): void {
    if (this.deleteEnabled && this.selectedOrder){
      if (!confirm('Czy na pewno usunąć zlecenie ' +
        `${this.selectedOrder.sourceAddress.city} - ${this.selectedOrder.destinationAddress.city}`)){
        return;
      }
      this.orders = this.orderService.delete(this.selectedOrder);
      this.selectedOrder = {} as Order;
    }
  }

  orderChanged(order: Order): void {
    if (order){
      this.orders = this.orderService.save(order);
      this.selectedOrder = {} as Order;
    }
  }
}
