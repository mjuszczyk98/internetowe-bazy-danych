import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { RowSelectEventArgs } from '@syncfusion/ej2-angular-grids';
import { Order } from 'src/app/models';

@Component({
  selector: 'app-order-table',
  templateUrl: './table.component.html'
})
export class TableComponent {
  @Input() orders: Order[];
  @Output() selectedOrder: EventEmitter<Order> = new EventEmitter();

  selectOrder(row: RowSelectEventArgs): void {
    this.selectedOrder.emit(row.data as Order);
  }
}
