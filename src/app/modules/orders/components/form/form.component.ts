import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Address, Contractor, Driver, DrivingLicenseType, Good, Order, User } from 'src/app/models';

@Component({
  selector: 'app-order-form',
  templateUrl: './form.component.html'
})
export class FormComponent implements OnChanges {
  @Input() order: Order;
  @Input() drivers: Driver[];
  @Input() goods: Good[];
  @Input() contractors: Contractor[];
  @Input() user: User;
  @Input() editEnabled: boolean;

  @Output() orderChanged: EventEmitter<Order> = new EventEmitter();

  orderForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private toastr: ToastrService) {
    this.setUpFrom();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.order) {
      this.setUpFrom();
    }
  }

  onSubmit(): void {
    const value = this.orderForm.value;

    if (!this.orderForm.valid){
      this.toastr.warning('Należy wypełnić wszystkie niezbędne wartości przed zapisem', 'Wstrzymano zapis');
      return;
    }

    if (this.orderForm.pristine) {
      this.toastr.warning('Należy dokonać zmian przed zapisem', 'Wstrzymano zapis');
      return;
    }

    const sourceAddress = {
      id: value.addressId1,
      street: value.addressStreet1,
      number: value.addressNumber1,
      localNumber: value.addressLocalNumber1,
      zipCode: value.addressZipCode1,
      city: value.addressCity1
    } as Address;

    const destinationAddress = {
      id: value.addressId2,
      street: value.addressStreet2,
      number: value.addressNumber2,
      localNumber: value.addressLocalNumber2,
      zipCode: value.addressZipCode2,
      city: value.addressCity2
    } as Address;

    const order = {
      id: value.id,
      sourceAddress,
      destinationAddress,
      shipmentDate: value.shipmentDate,
      deliveryDate: value.deliveryDate,
      weight: value.weight,
      palletCounter: value.palletCounter,
      price: value.price,
      contractor: this.contractors?.find(x => +x.id === +value.contractor),
      good: this.goods?.find(x => +x.id === +value.good),
      driver: this.drivers?.find(x => +x.id === +value.driver),
      user: this.user
    } as Order;

    this.orderChanged.emit(order);
  }

  private setUpFrom(): void {
    const group = {} as any;
    group.id = this.order?.id ?? undefined;
    group.shipmentDate = [this.order?.shipmentDate ?? '', Validators.required];
    group.deliveryDate = [this.order?.deliveryDate ?? '', Validators.required];
    group.weight = [this.order?.weight ?? '', Validators.required];
    group.palletCounter = [this.order?.palletCounter ?? '', Validators.required];
    group.price = [this.order?.price ?? '', Validators.required];
    group.contractor = [this.order?.contractor?.id ?? '', Validators.required];
    group.good = [this.order?.good?.id ?? '', Validators.required];
    group.driver = [this.order?.driver?.id ?? '', Validators.required];

    group.addressId1 = this.order?.sourceAddress?.id ?? '';
    group.addressStreet1 = [this.order?.sourceAddress?.street ?? '', Validators.required];
    group.addressNumber1 = [this.order?.sourceAddress?.number ?? '', Validators.required];
    group.addressLocalNumber1 = this.order?.sourceAddress?.localNumber ?? '';
    group.addressZipCode1 = [this.order?.sourceAddress?.zipCode ?? '', Validators.required];
    group.addressCity1 = [this.order?.sourceAddress?.city ?? '', Validators.required];

    group.addressId2 = this.order?.destinationAddress?.id ?? '';
    group.addressStreet2 = [this.order?.destinationAddress?.street ?? '', Validators.required];
    group.addressNumber2 = [this.order?.destinationAddress?.number ?? '', Validators.required];
    group.addressLocalNumber2 = this.order?.destinationAddress?.localNumber ?? '';
    group.addressZipCode2 = [this.order?.destinationAddress?.zipCode ?? '', Validators.required];
    group.addressCity2 = [this.order?.destinationAddress?.city ?? '', Validators.required];

    this.orderForm = this.formBuilder.group(group);
  }
}
