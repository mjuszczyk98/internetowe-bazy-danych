import { Component, OnInit } from '@angular/core';
import { RowSelectEventArgs } from '@syncfusion/ej2-angular-grids';
import { Observable, Subject } from 'rxjs';
import { Adr, Good } from 'src/app/models';
import { GoodService } from 'src/app/services/good.service';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-good',
  templateUrl: './good.component.html'
})
export class GoodComponent implements OnInit {
  goods: Observable<Good[]>;
  adrs: Observable<Adr[]>;
  selectedGood: Good;
  selectedAdr: Adr;
  deleteEnabled = false;
  editEnabled: Subject<boolean>;

  constructor(
    private goodService: GoodService,
    private loginService: LoginService,
  ) {
    this.selectedGood = {} as Good;
  }

  ngOnInit(): void {
    this.goods = this.goodService.get();
    this.adrs = this.goodService.getAdrs();
    this.editEnabled = this.loginService.getSubject();
  }

  selectGood(good: Good): void {
    this.selectedGood = good;
    this.deleteEnabled = true;
  }

  selectAdr(row: RowSelectEventArgs): void {
    this.selectedAdr = row.data as Adr;
  }

  addNew(): void {
    this.selectedGood = {} as Good;
    this.deleteEnabled = false;
  }

  delete(): void {
    if (this.deleteEnabled && this.selectedGood){
      if (!confirm(`Czy na pewno usunąć ładunek ${this.selectedGood.name}`)){
        return;
      }
      this.goods = this.goodService.delete(this.selectedGood);
      this.selectedGood = {} as Good;
    }
  }

  goodChanged(good: Good): void {
    if (good){
      this.goods = this.goodService.save(good);
      this.selectedGood = {} as Good;
    }
  }
}
