import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { RowSelectEventArgs } from '@syncfusion/ej2-angular-grids';
import { Good } from 'src/app/models';

@Component({
  selector: 'app-good-table',
  templateUrl: './table.component.html'
})
export class TableComponent {
  @Input() goods: Good[];
  @Output() selectedGood: EventEmitter<Good> = new EventEmitter();

  selectGood(row: RowSelectEventArgs): void {
    this.selectedGood.emit(row.data as Good);
  }
}
