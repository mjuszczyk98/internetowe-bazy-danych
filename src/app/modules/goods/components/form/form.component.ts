import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Adr, Good } from 'src/app/models';

@Component({
  selector: 'app-good-form',
  templateUrl: './form.component.html'
})
export class FormComponent implements OnChanges {
  @Input() good: Good;
  @Input() adrs: Adr[];
  @Input() editEnabled: boolean;

  @Output() goodChanged: EventEmitter<Good> = new EventEmitter();

  goodForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private toastr: ToastrService) {
    this.setUpFrom();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.good) {
      this.setUpFrom();
    }
  }

  onSubmit(): void {
    const value = this.goodForm.value;

    if (!this.goodForm.valid){
      this.toastr.warning('Należy wypełnić wszystkie niezbędne wartości przed zapisem', 'Wstrzymano zapis');
      return;
    }

    if (this.goodForm.pristine) {
      this.toastr.warning('Należy dokonać zmian przed zapisem', 'Wstrzymano zapis');
      return;
    }

    const good = {
      id: value.id,
      name: value.name,
      adr: this.adrs.find(x => +x.id === +value.adr),
    } as Good;

    this.goodChanged.emit(good);
  }

  private setUpFrom(): void {
    const group = {} as any;
    group.id = this.good?.id ?? undefined;
    group.name = [this.good?.name ?? '', Validators.required];
    group.adr = this.good?.adr?.id ?? '';

    this.goodForm = this.formBuilder.group(group);
  }
}
