import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Contractor } from 'src/app/models';
import { ContractorService } from 'src/app/services/constactor.service';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-contractor',
  templateUrl: './contractor.component.html'
})
export class ContractorComponent implements OnInit {
  contractors: Observable<Contractor[]>;
  selectedContractor: Contractor;
  deleteEnabled = false;
  editEnabled: Subject<boolean>;

  constructor(
    private contractorService: ContractorService,
    private loginService: LoginService,
  ) {
    this.selectedContractor = {} as Contractor;
  }

  ngOnInit(): void {
    this.contractors = this.contractorService.get();
    this.editEnabled = this.loginService.getSubject();
  }

  selectContractor(contractor: Contractor): void {
    this.selectedContractor = contractor;
    this.deleteEnabled = true;
  }

  addNew(): void {
    this.selectedContractor = {} as Contractor;
    this.deleteEnabled = false;
  }

  delete(): void {
    if (this.deleteEnabled && this.selectedContractor){
      if (!confirm(`Czy na pewno usunąć ładunek ${this.selectedContractor.name}`)){
        return;
      }
      this.contractors = this.contractorService.delete(this.selectedContractor);
      this.selectedContractor = {} as Contractor;
    }
    this.selectedContractor = null;
  }

  contractorChanged(contractor: Contractor): void {
    if (contractor){
      this.contractors = this.contractorService.save(contractor);
      this.selectedContractor = {} as Contractor;
    }
  }
}
