import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { RowSelectEventArgs } from '@syncfusion/ej2-angular-grids';
import { Contractor } from 'src/app/models';

@Component({
  selector: 'app-contractor-table',
  templateUrl: './table.component.html'
})
export class TableComponent {
  @Input() contractors: Contractor[];
  @Output() selectedContractor: EventEmitter<Contractor> = new EventEmitter();

  selectContractor(row: RowSelectEventArgs): void {
    this.selectedContractor.emit(row.data as Contractor);
  }
}
