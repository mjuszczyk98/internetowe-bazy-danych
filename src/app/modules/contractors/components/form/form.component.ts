import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Contractor, Address } from 'src/app/models';

@Component({
  selector: 'app-contractor-form',
  templateUrl: './form.component.html'
})
export class FormComponent implements OnChanges {
  @Input() contractor: Contractor;
  @Input() editEnabled: boolean;

  @Output() contractorChanged: EventEmitter<Contractor> = new EventEmitter();

  contractorForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private toastr: ToastrService) {
    this.setUpFrom();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.contractor) {
      this.setUpFrom();
    }
  }

  onSubmit(): void {
    const value = this.contractorForm.value;

    if (!this.contractorForm.valid){
      this.toastr.warning('Należy wypełnić wszystkie niezbędne wartości przed zapisem', 'Wstrzymano zapis');
      return;
    }

    if (this.contractorForm.pristine) {
      this.toastr.warning('Należy dokonać zmian przed zapisem', 'Wstrzymano zapis');
      return;
    }

    const address = {
      id: value.addressId,
      street: value.addressStreet,
      number: value.addressNumber,
      localNumber: value.addressLocalNumber,
      zipCode: value.addressZipCode,
      city: value.addressCity
    } as Address;

    const contractor = {
      id: value.id,
      name: value.name,
      address,
      nip: value.nip,
      phone: value.phone,
      email: value.email,
    } as Contractor;

    this.contractorChanged.emit(contractor);
  }

  private setUpFrom(): void {
    const group = {} as any;
    group.id = this.contractor?.id ?? undefined;
    group.name = [this.contractor?.name ?? '', Validators.required];
    group.nip = [this.contractor?.nip ?? '', Validators.required];
    group.phone = [this.contractor?.phone ?? '', Validators.required];
    group.email = [this.contractor?.email ?? '', Validators.required];

    group.addressId = this.contractor?.address?.id ?? '';
    group.addressStreet = [this.contractor?.address?.street ?? '', Validators.required];
    group.addressNumber = [this.contractor?.address?.number ?? '', Validators.required];
    group.addressLocalNumber = this.contractor?.address?.localNumber ?? '';
    group.addressZipCode = [this.contractor?.address?.zipCode ?? '', Validators.required];
    group.addressCity = [this.contractor?.address?.city ?? '', Validators.required];

    this.contractorForm = this.formBuilder.group(group);
  }
}
