import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Address, Driver, DrivingLicenseType } from 'src/app/models';

@Component({
  selector: 'app-driver-form',
  templateUrl: './form.component.html'
})
export class FormComponent implements OnChanges {
  @Input() driver: Driver;
  @Input() editEnabled: boolean;

  @Output() driverChanged: EventEmitter<Driver> = new EventEmitter();

  driverForm: FormGroup;
  drivingLicenseTypes: string[] = ['C', 'C+E'];
  genders: string[] = ['Mężczyzna', 'Kobieta'];

  constructor(private formBuilder: FormBuilder, private toastr: ToastrService) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.driver) {
      this.setUpFrom();
    }
  }

  onSubmit(): void {
    const value = this.driverForm.value;

    if (!this.driverForm.valid){
      this.toastr.warning('Należy wypełnić wszystkie niezbędne wartości przed zapisem', 'Wstrzymano zapis');
      return;
    }

    if (this.driverForm.pristine) {
      this.toastr.warning('Należy dokonać zmian przed zapisem', 'Wstrzymano zapis');
      return;
    }

    const address = {
      id: value.addressId,
      street: value.addressStreet,
      number: value.addressNumber,
      localNumber: value.addressLocalNumber,
      zipCode: value.addressZipCode,
      city: value.addressCity
    } as Address;

    const driver = {
      id: value.id,
      name: value.name,
      surname: value.surname,
      gender: value.gender,
      address,
      documentId: value.documentId,
      drivingLicense: value.drivingLicense,
      drivingLicenseId: value.drivingLicenseId,
      phone: value.phone,
    } as Driver;

    this.driverChanged.emit(driver);
  }

  private setUpFrom(): void {
    const group = {} as any;
    group.id = this.driver?.id ?? undefined;
    group.name = [this.driver?.name ?? '', Validators.required];
    group.surname = [this.driver?.surname ?? '', Validators.required];
    group.gender = [this.driver?.gender ?? this.genders[0], Validators.required];
    group.documentId = [this.driver?.documentId ?? '', Validators.required];
    group.drivingLicenseId = [this.driver?.drivingLicenseId ?? '', Validators.required];
    group.phone = [this.driver?.phone ?? '', Validators.required];
    group.drivingLicense = [this.driver?.drivingLicense ?? 'C', Validators.required];

    group.addressId = this.driver?.address?.id ?? '';
    group.addressStreet = [this.driver?.address?.street ?? '', Validators.required];
    group.addressNumber = [this.driver?.address?.number ?? '', Validators.required];
    group.addressLocalNumber = this.driver?.address?.localNumber ?? '';
    group.addressZipCode = [this.driver?.address?.zipCode ?? '', Validators.required];
    group.addressCity = [this.driver?.address?.city ?? '', Validators.required];

    this.driverForm = this.formBuilder.group(group);
  }
}
