import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { RowSelectEventArgs } from '@syncfusion/ej2-angular-grids';
import { Driver } from 'src/app/models';

@Component({
  selector: 'app-driver-table',
  templateUrl: './table.component.html'
})
export class TableComponent {
  @Input() drivers: Driver[];
  @Output() selectedDriver: EventEmitter<Driver> = new EventEmitter();

  selectDriver(row: RowSelectEventArgs): void {
    this.selectedDriver.emit(row.data as Driver);
  }
}
