import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Driver } from 'src/app/models';
import { DriverService } from 'src/app/services/driver.service';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-driver',
  templateUrl: './driver.component.html'
})
export class DriverComponent implements OnInit {
  drivers: Observable<Driver[]>;
  selectedDriver: Driver;
  deleteEnabled = false;
  editEnabled: Subject<boolean>;

  constructor(
    private driverService: DriverService,
    private loginService: LoginService,
  ) {
    this.selectedDriver = {} as Driver;
  }

  ngOnInit(): void {
    this.drivers = this.driverService.get();
    this.editEnabled = this.loginService.getSubject();
  }

  selectDriver(driver: Driver): void {
    this.selectedDriver = driver;
    this.deleteEnabled = true;
  }

  addNew(): void {
    this.selectedDriver = {} as Driver;
    this.deleteEnabled = false;
  }

  delete(): void {
    if (this.deleteEnabled && this.selectedDriver){
      if (!confirm(`Czy na pewno usunąć ładunek ${this.selectedDriver.name} ${this.selectedDriver.surname}`)){
        return;
      }
      this.drivers = this.driverService.delete(this.selectedDriver);
      this.selectedDriver = {} as Driver;
    }
  }

  driverChanged(driver: Driver): void {
    if (driver){
      this.drivers = this.driverService.save(driver);
      this.selectedDriver = {} as Driver;
    }
  }
}
