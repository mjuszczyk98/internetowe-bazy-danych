import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Address, User } from 'src/app/models';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html'
})
export class AuthComponent implements OnInit, OnDestroy {
  authForm: FormGroup;

  reutrnUrl: string;
  type: string;
  authSubscription: Subscription;

  constructor(
    private authService: AuthService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder) {}

  enableButton(): boolean {
    return this.authForm.valid && !this.authForm.pristine;
  }

  onSubmit(): void {
    const value = this.authForm.value;
    if (this.type === 'login') {
      const user = {
        username: value.login,
        password: value.password
      } as User;
      this.authService.login(user);

    } else if (this.type === 'register'){
      const userAddress = {
        street: value.addressStreet,
        number: value.addressNumber,
        localNumber: value.addressLocalNumber,
        zipCode: value.addressZipCode,
        city: value.addressCity
      } as Address;

      const user = {
        username: value.login,
        password: value.password,
        name: value.name,
        surname: value.surname,
        address: userAddress,
        privileges: !!value.privlages ? 'FULL' : 'READ',
      } as User;
      this.authService.register(user);
    }
  }

  ngOnInit(): void {
    this.reutrnUrl = this.route.snapshot.queryParams['returnUrl' as string] || '/';
    this.type = this.route.snapshot.paramMap.get('type');

    this.authSubscription = this.authService.getSubject().subscribe(x => {
      if (!!x && this.type !== 'register') {
        this.router.navigate([this.reutrnUrl]);
      }
    });

    const group: any = {} as any;
    group.login = ['', Validators.required];
    group.password = ['', Validators.required];

    group.name = '';
    group.surname = '';
    group.privilages = '';

    group.addressStreet = '';
    group.addressNumber = '';
    group.addressLocalNumber = '';
    group.addressZipCode = '';
    group.addressCity = '';

    this.authForm = this.formBuilder.group(group);
  }

  ngOnDestroy(): void {
    this.authSubscription.unsubscribe();
  }

}
