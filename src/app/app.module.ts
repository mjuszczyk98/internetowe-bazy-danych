import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './container/app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DriverModule } from './modules/driver/driver.module';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule} from '@angular/material/icon';
import { MatButtonModule} from '@angular/material/button';
import { ContractorModule } from './modules/contractors/contractor.module';
import { GoodModule } from './modules/goods/good.module';
import { OrderModule } from './modules/orders/order.module';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { FilterService, PageService, SearchService, SortService, ToolbarService } from '@syncfusion/ej2-angular-grids';
import { AuthService } from './services/auth.service';
import { AuthGuard } from './services/auth.guard';
import { TokenInterceptor } from './interceptors/token.interceptor';
import { ErrorInterceptor } from './interceptors/error.interceptor';
import { AuthModule } from './modules/auth/auth.module';
import { LoginService } from './services/login.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AuthModule,
    DriverModule,
    ContractorModule,
    GoodModule,
    OrderModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    HttpClientModule,
    ToastrModule.forRoot({
      preventDuplicates: true,
    })
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: (login: LoginService) => () => login.saveUser(),
      deps: [LoginService],
      multi: true
    },
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true},
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
    AuthService,
    AuthGuard,
    SearchService,
    SortService,
    FilterService,
    PageService,
    ToolbarService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
