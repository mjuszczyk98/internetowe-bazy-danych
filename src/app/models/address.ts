export interface Address {
    id: number;
    street: string;
    number: string;
    localNumber: string;
    zipCode: string;
    city: string;
}
