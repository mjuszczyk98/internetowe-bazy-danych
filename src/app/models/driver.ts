import { Address } from './address';

export interface Driver {
    id: number;
    name: string;
    surname: string;
    gender: string;
    address: Address;
    documentId: string;
    drivingLicense: string;
    drivingLicenseId: string;
    phone: string;
}
