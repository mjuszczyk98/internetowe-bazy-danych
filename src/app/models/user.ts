import { Address } from './address';

export interface User {
    id: number;
    username: string;
    name: string;
    surname: string;
    address: Address;
    privileges: string;
    password: string;
}
