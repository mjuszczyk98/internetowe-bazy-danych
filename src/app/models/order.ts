import { Address } from './address';
import { Contractor } from './contractor';
import { Driver } from './driver';
import { Good } from './good';
import { User } from './user';

export interface Order {
    id: number;
    sourceAddress: Address;
    destinationAddress: Address;
    shipmentDate: Date | string;
    deliveryDate: Date | string;
    contractor: Contractor;
    good: Good;
    description: string;
    weight: number;
    palletCounter: number;
    price: number;
    driver: Driver;
    user: User;
}
