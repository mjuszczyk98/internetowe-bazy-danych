export interface Good {
    id: number;
    name: string;
    adr: Adr;
}

export interface Adr {
    id: number;
    number: number;
    description: string;
}
