import { Address } from './address';

export interface Contractor {
    id: number;
    name: string;
    address: Address;
    nip: string;
    phone: string;
    email: string;
}
