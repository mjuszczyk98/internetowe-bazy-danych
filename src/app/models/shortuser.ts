export interface ShortUser {
    id: number;
    username: string;
    privileges: string;
    token: string;
}
