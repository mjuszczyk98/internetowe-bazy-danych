import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {

  canEdit: Subject<boolean>;

  constructor(private authService: AuthService, private loginService: LoginService) {}

  authenticated: Subject<boolean>;

  ngOnInit(): void {
    this.authenticated = this.authService.getSubject();
    this.authService.requestSubjectValue();
    this.canEdit = this.loginService.getSubject();
  }

  logout(): void {
    this.loginService.logout();
  }
}
