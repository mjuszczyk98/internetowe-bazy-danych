import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Address } from '../models';
import { catchError, map, tap } from 'rxjs/operators';
import { Contractor } from '../models/contractor';
import { HttpClient } from '@angular/common/http';
import { API_URL } from 'src/assets/config';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class ContractorService {
  private contractors: Contractor[];

  constructor(private http: HttpClient, private toastr: ToastrService) {
  }

  public get(): Observable<Contractor[]> {
    return this.http.get<Contractor[]>(API_URL + '/contractor').pipe(
      tap(x => {
        this.contractors = x;
      }),
      catchError( x => {
        this.toastr.error('Niepowodzenie podczas pobierania kontrahentów.', 'Błąd!');
        return of (this.contractors);
      })
    );
  }

  public save(contractor: Contractor): Observable<Contractor[]> {

    if (contractor.id) {
      return this.http.put<Contractor>(API_URL + '/contractor', contractor).pipe(
        map(x => {
          const index = this.contractors.findIndex(d => d.id === x.id);
          if (index !== -1) {
            this.contractors[index] = x;
          }
          this.toastr.success('Pomyślnie zaktualizowano dane kontrahenta.', 'Sukces!');
          return this.contractors;
        }),
        catchError( x => {
          this.toastr.error('Niepowodzenie podczas edycji danych kontrahenta.', 'Błąd!');
          return of (this.contractors);
        })
      );
    }
    else {
      return this.http.post<Contractor>(API_URL + '/contractor', contractor).pipe(
        map(x => {
          this.contractors = [...this.contractors, x];
          this.toastr.success('Pomyślnie dodano kontrahenta.', 'Sukces!');
          return this.contractors;
        }),
        catchError( x => {
          this.toastr.error('Niepowodzenie podczas dodawania kontrahenta.', 'Błąd!');
          return of (this.contractors);
        })
      );
    }
  }

  public delete(contractor: Contractor): Observable<Contractor[]> {
    return this.http.delete<Contractor>(API_URL + '/contractor/' + contractor.id).pipe(
      map(x => {
        this.contractors = this.contractors.filter(d => d.id !== x.id);
        this.toastr.success('Pomyślnie usunięto kontrahenta.', 'Sukces!');
        return this.contractors;
      }),
      catchError( x => {
        this.toastr.error('Niepowodzenie podczas usuwania kontrahenta.', 'Błąd!');
        return of (this.contractors);
      })
    );
  }
}
