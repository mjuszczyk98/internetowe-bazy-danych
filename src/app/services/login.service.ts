import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { API_URL } from 'src/assets/config';
import { User } from '../models/user';

@Injectable({
    providedIn: 'root'
  })
  export class LoginService {
    private user: User;
    private subject: Subject<boolean> = new BehaviorSubject(false);

    constructor(private http: HttpClient) {
    }

    public getSubject(): Subject<boolean> {
      return this.subject;
    }

    public canEdit(): boolean {
      const ret = this.user?.privileges === 'FULL' ? true : false
      this.subject.next(ret);
      return ret;
    }

    public saveUser(): void {
      if (!!sessionStorage.getItem('user_id')){
        this.http.get<User>(API_URL + '/user/' + sessionStorage.getItem('user_id')).toPromise().then((user) => {
          this.user = user;
          this.canEdit();
        }).catch((err) => {
          console.log(err);
        });
      }
    }

    public logout(): void {
      sessionStorage.removeItem('user_id');
      sessionStorage.removeItem('user_token');
      sessionStorage.removeItem('user_username');
    }

    public getUser(): User {
        return this.user;
    }
  }
