import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { API_URL } from 'src/assets/config';
import { catchError, map, tap } from 'rxjs/operators';
import { Adr, Good } from '../models/good';
import { ToastrService } from 'ngx-toastr';

@Injectable({
    providedIn: 'root'
  })
  export class GoodService {
    private goods: Good[];
    private adrs: Adr[];

    constructor(private http: HttpClient, private toastr: ToastrService) {
    }

    public get(): Observable<Good[]> {
      return this.http.get<Good[]>(API_URL + '/good').pipe(
        tap(x => {
          this.goods = x;
          })
      );
    }

    public getAdrs(): Observable<Adr[]> {
      return this.http.get<Adr[]>(API_URL + '/adr').pipe(
        tap(x => {
          this.adrs = x;
          }),
          catchError( x => {
            this.toastr.error('Niepowodzenie podczas pobierania towarów.', 'Błąd!');
            return of (this.adrs);
          })
      );
    }

    public save(good: Good): Observable<Good[]> {
      if (good.id) {
        return this.http.put<Good>(API_URL + '/good', good).pipe(
          map(x => {
            const index = this.goods.findIndex(d => d.id === x.id);
            if (index !== -1) {
              this.goods[index] = x;
            }
            this.toastr.success('Pomyślnie edytowano towar', 'Sukces!');
            return this.goods;
          }),
          catchError( x => {
            this.toastr.error('Niepowodzenie podczas edycji towaru.', 'Błąd!');
            return of (this.goods);
          })
        );
      }
      else {
        return this.http.post<Good>(API_URL + '/good', good).pipe(
          map(x => {
            this.goods = [...this.goods, x];
            this.toastr.success('Pomyślnie dodano towar', 'Sukces!');
            return this.goods;
          }),
          catchError( x => {
            this.toastr.error('Niepowodzenie podczas dadawania towaru.', 'Błąd!');
            return of (this.goods);
          })
        );
      }
    }

    public delete(good: Good): Observable<Good[]> {
      return this.http.delete<Good>(API_URL + '/good/' + good.id).pipe(
        map(x => {
          this.goods = this.goods.filter(d => d.id !== x.id);
          this.toastr.success('Pomyślnie usunięto towar', 'Sukces!');
          return this.goods;
        }),
        catchError( x => {
          this.toastr.error('Niepowodzenie podczas usuwania towaru.', 'Błąd!');
          return of (this.goods);
        })
      );
    }
  }
