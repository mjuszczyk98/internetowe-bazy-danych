import { Injectable } from '@angular/core';
import { User } from 'src/app/models';
import { API_URL } from 'src/assets/config';
import { BehaviorSubject, of, Subject } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';
import { LoginService } from './login.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private subject: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(private http: HttpClient, private toastr: ToastrService, private loginService: LoginService) {}

  public login(user: User): void{

    this.http.post<any>(API_URL + '/login', {
     username: user.username,
     password: user.password
   }, {observe: 'response'} ).toPromise().then( resp =>
     {
       this.saveToken(resp.headers.get('Authorization'), resp.headers.get('UserID'), resp.headers.get('Username'));
       this.toastr.success(`Witaj ${sessionStorage.getItem('user_username')}`, 'Sukces!');
       this.loginService.saveUser();
       this.subject.next(true);
     }
   ).catch((err) => {
    this.toastr.error('Błąd podczas logowania', 'Błąd!');
   });
  }

  public register(user: User): void {
    this.http.post<User>(API_URL + '/user', user).toPromise().then(x => {
      this.toastr.success('Pomyślnie dodano użytkownika', 'Sukces!');
    })
    .catch(x => this.toastr.error('Błąd podczas rejestracji', 'Błąd!'));
  }

  public getSubject(): Subject<boolean> {
    return this.subject;
  }

  public requestSubjectValue(): void {
    this.subject.next(!!this.getToken());
  }

  public saveToken(token: string, id: string, username: string): void{
    this.clearToken();
    window.sessionStorage.setItem('user_token', token);
    window.sessionStorage.setItem('user_id', id);
    window.sessionStorage.setItem('user_username', username);
  }

  public clearToken(): void {
    this.subject.next(false);
    window.sessionStorage.removeItem('user_token');
    window.sessionStorage.removeItem('user_id');
    window.sessionStorage.removeItem('user_username');
  }

  public getToken(): string {
    return sessionStorage.getItem('user_token');
  }
}
