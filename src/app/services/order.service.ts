import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { API_URL } from 'src/assets/config';
import { Address, Contractor, Order, Good, User } from '../models';
import { catchError, map, tap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { LoginService } from './login.service';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  private orders: Order[];

  constructor(private http: HttpClient, private toastr: ToastrService, private login: LoginService) {
  }

  public get(): Observable<Order[]> {
    return this.http.get<Order[]>(API_URL + '/order').pipe(
      tap(x => {
        this.orders = x;
      }),
      catchError( x => {
        this.toastr.error('Niepowodzenie podczas pobierania zleceń.', 'Błąd!');
        return of (this.orders);
      })
    );
  }

  public save(order: Order): Observable<Order[]> {
    order.user = this.login.getUser();
    if (order.id) {
      return this.http.put<Order>(API_URL + '/order', order).pipe(
        map(x => {
          const index = this.orders.findIndex(d => d.id === x.id);
          if (index !== -1) {
            this.orders[index] = x;
          }
          this.toastr.success('Pomyślnie edytowano zlecenie.', 'Sukces!');
          return this.orders;
        }),
        catchError( x => {
          this.toastr.error('Niepowodzenie podczas edytowawnia zlecenia.', 'Błąd!');
          return of (this.orders);
        })
      );
    }
    else {
      return this.http.post<Order>(API_URL + '/order', order).pipe(
        map(x => {
          this.orders = [...this.orders, x];
          this.toastr.success('Pomyślnie dodano zlecenie.', 'Sukces!');
          return this.orders;
        }),
        catchError( x => {
          this.toastr.error('Niepowodzenie podczas dodawania zlecenia.', 'Błąd!');
          return of (this.orders);
        })
      );
    }
  }

  public delete(order: Order): Observable<Order[]> {
    return this.http.delete<Order>(API_URL + '/order/' + order.id).pipe(
      map(x => {
        this.orders = this.orders.filter(d => d.id !== x.id);
        this.toastr.success('Pomyślnie usunięto zlecenie.', 'Sukces!');
        return this.orders;
      }),
      catchError( x => {
        this.toastr.error('Niepowodzenie podczas usuwania zlecenia.', 'Błąd!');
        return of (this.orders);
      })
    );
  }
}
