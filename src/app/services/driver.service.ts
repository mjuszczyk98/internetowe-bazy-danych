import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { API_URL } from 'src/assets/config';
import { Driver } from '../models/driver';

@Injectable({
  providedIn: 'root'
})
export class DriverService {
  private drivers: Driver[];

  constructor(private http: HttpClient, private toastr: ToastrService) {
  }

  public get(): Observable<Driver[]> {
    return this.http.get<Driver[]>(API_URL + '/driver').pipe(
      tap(x => {
        this.drivers = x;
        }),
        catchError( x => {
          this.toastr.error('Niepowodzenie podczas pobierania kierowców.', 'Błąd!');
          return of (this.drivers);
        })
    );
  }

  public save(driver: Driver): Observable<Driver[]> {

    if (driver.id) {
      return this.http.put<Driver>(API_URL + '/driver', driver).pipe(
        map(x => {
          const index = this.drivers.findIndex(d => d.id === x.id);
          if (index !== -1) {
            this.drivers[index] = x;
          }
          this.toastr.success('Pomyślnie zaktualizowano dane kierowcy', 'Sukces');
          return this.drivers;
        }),
        catchError( x => {
          this.toastr.error('Niepowodzenie podczas edycji kierowcy.', 'Błąd!');
          return of (this.drivers);
        })
      );
    }
    else {
      return this.http.post<Driver>(API_URL + '/driver', driver).pipe(
        map( x => {
          this.drivers = [...this.drivers, x];
          this.toastr.success('Pomyślnie dodano kierowcę', 'Sukces');
          return this.drivers;
          }),
          catchError( x => {
            this.toastr.error('Niepowodzenie podczas dodawania kierowcy.', 'Błąd!');
            return of (this.drivers);
          })
      );
    }
  }

  public delete(driver: Driver): Observable<Driver[]> {
    return this.http.delete<Driver>(API_URL + '/driver/' + driver.id).pipe(
      map( x => {
        this.drivers = this.drivers.filter(d => d.id !== x.id);
        this.toastr.success('Pomyślnie usunięto kierowcę', 'Sukces');
        return this.drivers;
        }),
        catchError( x => {
          this.toastr.error('Niepowodzenie podczas usuwania kierowcy.', 'Błąd!');
          return of (this.drivers);
        })
    );
  }
}
