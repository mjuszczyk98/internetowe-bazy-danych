import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth.service';
import { LoginService } from './login.service';

@Injectable({providedIn: 'root'})
export class AuthGuard implements CanActivate {
    constructor(private authService: AuthService, private router: Router, private loginService: LoginService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        if (state.url.match(/\/auth/)){
            const type = route.paramMap.get('type');
            if (type !== 'login' && type !== 'register') {
                this.router.navigate(['/auth/login']);
                return false;
            }

            if ((!this.authService.getToken() && type !== 'register') ||
                (this.authService.getToken() && type === 'register' && this.loginService.canEdit())) {
                return true;
            }

            this.router.navigate(['/']);
            return false;
        }

        if (!!this.authService.getToken()) {
            return true;
        }
        this.authService.requestSubjectValue();
        this.router.navigate(['/auth/login'], {queryParams: { returnUrl: state.url } });
        return false;
    }
}
