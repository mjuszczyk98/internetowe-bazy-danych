import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthComponent } from './modules/auth/container/auth.component';
import { ContractorComponent } from './modules/contractors/container/contractor.component';
import { DriverComponent } from './modules/driver/container/driver.component';
import { GoodComponent } from './modules/goods/container/good.component';
import { OrderComponent } from './modules/orders/container/order.component';
import { AuthGuard } from './services/auth.guard';

const routes: Routes = [
  { path: 'auth/:type', component: AuthComponent, canActivate: [AuthGuard] },
  { path: 'drivers', component: DriverComponent, canActivate: [AuthGuard] },
  { path: 'contractors', component: ContractorComponent, canActivate: [AuthGuard] },
  { path: 'goods', component: GoodComponent, canActivate: [AuthGuard] },
  { path: 'orders', component: OrderComponent, canActivate: [AuthGuard] },
  { path: '', redirectTo: '/orders', pathMatch: 'full' },
  { path: '**', redirectTo: '/orders', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
